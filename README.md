# yaz_cmd_perl_to_json
## Requires 
* `yaz_cmd_perl`
  * `git clone --recurse-submodules https://github.com/nkay08/yaz_cmd_perl_to_json.git`
    * (then set `git config submodule.recurse true`)
  *  or get from [https://github.com/nkay08/yaz_cmd_perl](https://github.com/nkay08/yaz_cmd_perl)

* needs libyaz from yaz toolkit
  * [https://www.indexdata.com/resources/software/yaz/](https://www.indexdata.com/resources/software/yaz/)
  * [http://ftp.indexdata.dk/pub/yaz/yaz-5.27.1.tar.gz](http://ftp.indexdata.dk/pub/yaz/yaz-5.27.1.tar.gz)
    * `./buildconf.sh`
    * `./configure`
    * `make`
    * `sudo make install`
  * or
    * `./buildconf.sh`
    * `./configure --prefix=$HOME/myapps`
    * `make`
    * `make install`
    * ...
    * Add `export PATH="$HOME/myapps/bin:$PATH"` to `.bashrc`
    * (Reload file with `source ~/.bashrc`

## Usage
* `python3 yaz_cmd_perl_to_json.py <host> <port> <databaseName> <user> <password> <syntax> <query> (<query_type>) (<max_records>)`  
* `python3 yaz_cmd_perl_to_json.py --test <number_from_1-5>`  
* Syntax is either `MAB`or `USMARC`  
* Query in `pqf` `cql` `ccl` format 


## Usage in Python
* make sure there is a `yaz_cmd.pl` or a `yaz_cmd_packed` file, and the paths are set correctly, when not using a simple string
* `from yaz_cmd2json.yaz_cmd2json import get_yaz_json`  
