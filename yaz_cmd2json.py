#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import subprocess
import argparse, os
import re
import traceback
import chardet

PERL_PATH = ''
PERL_CMD_PATH = 'yaz_cmd'
PERL_CMD_EXE = 'yaz_cmd.pl'
PERL_CMD_PACKED_PATH=''
PERL_CMD_PACKED='yaz_cmd_packed'


def get_records_perl(host, port, dbname, user, password, syntax, query, query_type="", max_records=10, timeout=10000):
    """Get records from yaz_cmd_perl as raw stdout in a subprocess"""
    dirpath = os.path.dirname(__file__)
    cmdpath = os.path.join(os.path.join(dirpath, PERL_CMD_PATH), PERL_CMD_EXE)
    packedpath = os.path.join(os.path.join(dirpath, PERL_CMD_PACKED_PATH),PERL_CMD_PACKED)
    try:
        if os.path.isfile(packedpath):
            # print("binary")
            pipe = subprocess.run(
                [packedpath, str(host), str(port), str(dbname), str(user), str(password), str(syntax), str(query), str(query_type),
                 str(max_records)],
                stdout=subprocess.PIPE, timeout=timeout/1000)
            out = u''
            if isinstance(pipe.stdout, bytes):
                out = pipe.stdout
            else:
                for line in pipe.stdout:
                    out.join(line)
            # print(out)
            # raw = out[0]
            # TODO
            # split at "b'"??? NO?
            return out

        elif os.path.isfile(cmdpath):
            # print("perl")
            pipe = subprocess.run(["perl", cmdpath, str(host), str(port), str(dbname), str(user), str(password), str(syntax), str(query),str(query_type), str(max_records)],
                                        stdout=subprocess.PIPE, timeout=timeout/1000)
            out = u''
            if isinstance(pipe.stdout, bytes):
                out = pipe.stdout
            else:
                for line in pipe.stdout:
                    out.join(line)
            # print(out)
            # raw = out[0]
            #TODO
            #split at "b'"
            return out
        else:
            raise FileNotFoundError()
    except Exception as e:
        traceback.print_exc()
        raise e


def exchange_chars_utf8(rep_string):
    """Replaces misformed characters in MAB result"""
    #uses encoding similar to "ISO-8859-1"
    # mapping = [
    #     (chr(0x00C9)+"o", "ö"), (chr(0x00C9)+"O", "Ö"), (chr(0x00C9)+"a", "ä"),
    #            (chr(0x00C9)+"A", "Ä"), (chr(0x00C9)+"u", "ü"), (chr(0x00C9)+"U", "Ü"),
    #            (chr(137), ""), (chr(136), ""), (chr(251), "ß"), (chr(194)+"a", "á"),
    #            (chr(194)+"i", "í"), (chr(194)+"e", "é"), (chr(208)+"c", "ç"), (chr(194)+"E", "É"),
    #            (chr(207)+"c", "č"), (chr(207)+"s", "š"), (chr(207)+"S", "Š"), (chr(201)+"i", "ï"),
    #             (chr(200)+"e", "ë"), (chr(193)+"e", "è"), (chr(193)+"a", "à"), (chr(193)+"i", "ì"),
    #     (chr(193)+"o", "ò"), (chr(193)+"u", "ú"), (chr(195)+"u", "û"), (chr(201)+"e", "ë"),
    #     (chr(195)+chr(194), "ä"), ("&amp;#263;", "ć"), ("Ã¤", "ä")
    # ]
    mapping = [
        ("\\xc9"+"o", "ö"), ("\\xc9"+"O", "Ö"), ("\\xc9"+"a", "ä"),
        ("\xc9"+"A", "Ä"), ("\\xc9"+"u", "ü"), ("\\xc9"+"U", "Ü"),
        ("\\x89", ""), ("\\x88", ""), ("\\xfb", "ß"), ("\\xc2"+"a", "á"),
        ("\\xc2"+"i", "í"), ("\\xc2"+"e", "é"), ("\\xd0"+"c", "ç"), ("\\xc2"+"E", "É"),
        ("\\xcf"+"c", "č"), ("\\xcf"+"s", "š"), ("\\xcf"+"S", "Š"), ("\\xc9"+"i", "ï"),
        ("\\xc8"+"e", "ë"), ("\\xc1"+"e", "è"), ("\\xc1"+"a", "à"), ("\\xc1"+"i", "ì"),
        ("\\xc1"+"o", "ò"), ("\\xc1"+"u", "ú"), ("\\xc3"+"u", "û"), ("\\xc9"+"e", "ë"),
        ("\\xc3"+"\\xc2", "ä"), ("&amp;#263;", "ć"), ("Ã¤", "ä"), ("\\xb6", "¶"),
        ("\\xc3" + "e", "ê"), ("\\xc4" + "a", "ã")
    ]
    regex = [
        ("\\\\xc5(?=\w)", ""),
    ]

    rep_string = str(rep_string)

    for k, v in mapping:
        # rep_string = rep_string.replace(k, v)
        regex.append((k.replace('\\', '\\\\'), v))

    for k, v in regex:
        rep_string = re.sub(k, v, rep_string)

    return rep_string


def parse_record(record, syntax, extra_info=None):
    """Parse a set of yaz records of either MAB or USMARC syntax"""
    #split recordset at "\xc1". relevent parts are at [1]
    recordset = None
    split = str(record).split("\\x1c")
    if len(split) > 1:
        recordset = split[1]

    # list of records, with "<number>\n" at the start of each
    # last for MAB is empty
    list_of_records = None
    if recordset:
        list_of_records = recordset.split("\\x1d")[:-1]

    #accumulate resultset by appending corresponding result for each record in list_of_records
    resultset = []
    if list_of_records:
        if syntax.lower() == "mab":
            for line in list_of_records:
                resultset.append(get_mab_json(line, extra_info))
        elif syntax.lower() == "usmarc":
            for line in list_of_records:
                resultset.append(get_usmarc_json(line, extra_info))

    return resultset


def unescape(string):
    """ Unescape escaped unicode
        quote: 'Mind the triple-quote string and the dash right before the closing 3-quotes.
                1. Using a 3-quoted string will ensure that if the user enters ' \\" ' (spaces added for visual clarity) in the string it would not disrupt the evaluator;
                2. The dash at the end is a failsafe in case the user's string ends with a ' \" ' . Before we assign the result we slice the inserted dash with [:-1]

                So there would be no need to worry about what the users enter, as long as it is captured in raw format.'
                [https://stackoverflow.com/a/11281948]

        Can probably be replaced by .encode().decode('utf-8)
    """
    return eval('u"""' + string.replace('"', r'\"') + '-"""')[:-1]


def decode_mab(string):
    """ Decode malformed MAB string """
    string = exchange_chars_utf8(string)
    string = string.encode('utf-8').decode('utf-8')
    # string = unescape(string)
    # string = string.encode().decode('unicode-escape')
    return string.strip()


def get_mab_json(record, extra_info=None):
    """Parse raw record and create json for MAB syntax"""
    json_dict = {}
    if extra_info:
        for k, v in extra_info.items():
            json_dict[k] = v
    json_dict["syntax"] = "mab"
    # record = exchange_chars_utf8(record)
    num = re.findall("^\d+", record)
    json_dict["num"] = num[0]
    record = re.split("^\d+\\\\n", record)[1]
    fields = {}
    split = record.split("\\x1e")
    for i in range(0, len(split)-1):
        field = split[i][0:3]
        subfield = split[i][3:4]
        value = split[i][4:]
        valuesplit = value.split("\\x1f")
        # print(len(valuesplit), valuesplit)
        if (subfield != "" and subfield != " ") and len(valuesplit) <= 1:
            fields[field] = {subfield: decode_mab(value)}
        elif len(valuesplit) > 1:
            subfields = {}
            for j in range(1, len(valuesplit)-1):
                subfieldfield = valuesplit[j][0:1]
                subfieldvalue = valuesplit[j][1:]
                subfields[subfieldfield] = decode_mab(subfieldvalue)
            fields[field] = subfields
        else:
            # print(value)
            # print(value.encode('utf-8').decode('utf-8'))
            fields[field] = decode_mab(value)
    json_dict['fields'] = fields

    # print(json_dict)
    return json_dict

def decode_usmarc(string):
    string = string.encode('latin1').decode('unicode-escape').encode('latin1').decode('utf-8')
    # strin = string.encode().decode('utf-8')
    string = string.strip()
    return string

def get_usmarc_json(record, extra_info=None):
    """Parse raw record and create json for USMARC syntax"""
    json_dict = {}
    if extra_info:
        for k, v in extra_info.items():
            json_dict[k] = v
    json_dict['syntax'] = "usmarc"
    fields={}
    num = re.findall("^\d+", record)
    json_dict["num"] = num[0]
    split = str(record).split("\\n")
    for i in range(1, len(split)-4):
        field = split[i][0:3]
        value = split[i][3:]
        valuesplit = re.split(r'\$([a-zA-Z\d])', value)
        if len(valuesplit) > 1:
            subfields = {}
            j=1
            while j < len(valuesplit)-1:
                if(j+1) < len(valuesplit):
                    subfields[valuesplit[j]] = decode_usmarc(valuesplit[j+1])
                    j+=1
                j+=1
            fields[field] = subfields
        else:
            fields[field] = decode_usmarc(value)
    json_dict['fields'] = fields
    # print(json_dict)
    return json_dict


def get_yaz_json(host, port, dbname, user, password, syntax, query, query_type="PQF", max_records=10, timeout=10000):
    """Gets a raw yaz record and parses it accordingly to create a json"""

    if not max_records:
        max_records = 10
    if not query_type:
        query_type = "PQF"
    if not timeout:
        timeout = 10000

    raw = None
    if query_type is None:
        query_type="PQF"
    try:
        raw = get_records_perl(host, port, dbname, user, password, syntax, query, query_type, max_records, timeout)
        # print(chardet.detect(raw))
        # print(raw)
    except Exception as e:
        print(e)

    res = [{}]
    if raw:
        try:
            res = parse_record(raw, syntax, extra_info={"host": host, "query": query, "query_type": query_type, "syntax": syntax})
            # print(res)
        except Exception as e:
            print(e)
            traceback.print_exc()
            raise UnicodeError

    # print(res)
    return res

def get_yaz_json_from_string(text, syntax):
    res = [{}]
    if text:
        try:
            res = parse_record(text, syntax, extra_info={"syntax": syntax})
            # print(res)
        except Exception as e:
            print(e)
            traceback.print_exc()
            raise UnicodeError

    # print(res)
    return res


def get_yaz_json_test(test=1):
    """Testfunction that calls get_yaz_json() with preset arguments depending on @arg test"""

    host = ""
    port = ""
    dbname = ""
    user = ""
    password = ""
    syntax = ""
    query = ""
    query_type=None
    max_records = 5

    if int(test) == 1:
        host = "193.30.112.135"
        port = "9991"
        dbname = "HBZ01"
        user = ""
        password = ""
        syntax = "MAB"
        query = 'funktion'
    elif int(test) == 2:
        host = "bvbr.bib-bvb.de"
        port = "9991"
        dbname = ""
        user = ""
        password = "BVB89"
        syntax = "MAB"
        query = "funktion"
    elif int(test) == 3:
        host = "z3950.kobv.de"
        port = "9991"
        dbname = "U-KBV90"
        user = ""
        password = ""
        syntax = "USMARC"
        query = "funktion"
    elif int(test) == 4:
        host = ""
        port = ""
        dbname = ""
        user = ""
        password = ""
        syntax = ""
        query = ""
    elif int(test) == 5:
        host = "sru.gbv.de"
        port = "210"
        dbname = "gvkplus"
        user = ""
        password = ""
        syntax = "USMARC"
        query = '@attr 1=7 @attr 4=1 "1"'
    elif int(test) == 6:
      host = "193.30.112.135"
      port = "9991"
      dbname = "HBZ01"
      user = ""
      password = ""
      syntax = "MAB"
      query_type = "PQF"
      query = '@attr 1=4 @attr 4=1 "goethe"'
    elif int(test) == 7:
      host = "193.30.112.135"
      port = "9991"
      dbname = "HBZ01"
      user = ""
      password = ""
      syntax = "MAB"
      query_type = "CQL"
      query = 'title=dinosaur'
    elif int(test) == 8:
      host = "193.30.112.135"
      port = "9991"
      dbname = "HBZ01"
      user = ""
      password = ""
      syntax = "MAB"
      query_type = "CCL"
      query = 'ti=dinosaur'

    return get_yaz_json(host, port, dbname, user, password, syntax, query, query_type, max_records)


if __name__ == '__main__':
    """Commandline argument parser"""
    parser = argparse.ArgumentParser(description="Get results for yaz query as json")
    # parser.add_argument('strings', type=string, help='Strings for the accumulator')
    parser.add_argument('-s', '--string', nargs='?')
    parser.add_argument('-t', '--test', nargs='?')
    parser.add_argument('host', nargs='?')
    parser.add_argument('port', nargs='?')
    parser.add_argument('dbname', nargs='?')
    parser.add_argument('login', nargs='?')
    parser.add_argument('syntax', nargs='?')
    parser.add_argument('query', nargs='?')
    parser.add_argument('query_type', nargs='?')
    parser.add_argument('max_records', nargs='?')
    parser.add_argument('timeout', nargs='?')
    args = parser.parse_args()
    if(args.host and args.port and args.dbname and args.login and args.syntax and args.query):
        print(json.dumps(
            get_yaz_json(host=args.host, port=args.port, dbname=args.dbname, login=args.login, syntax=args.syntax,
                         query=args.query, query_type=args.query_type, max_records=args.max_records, timeout=args.timeout)))
    elif args.test:
        print(json.dumps(get_yaz_json_test(test=args.test)))
    elif args.string:
        if(args.host):
            print(json.dumps(json.dumps(get_yaz_json_from_string(args.string, args.host))))
        else:
            print('Please provide syntax')
